package br.com.fiap.si.receiveapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Interpolator;

public class ReceiverActivity extends AppCompatActivity {
    Intent intent = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
    }

    public void btnLogar(View view) {
        //Intent intent = new Intent();
        //intent.setAction("android.app.action.ACTION_PASSWORD_SUCCEEDED");
        //sendBroadcast(intent);

        intent = new Intent(this,Logger.class);
        startService(intent);

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void btnStop(View view) {
        stopService(intent);
    }
}
