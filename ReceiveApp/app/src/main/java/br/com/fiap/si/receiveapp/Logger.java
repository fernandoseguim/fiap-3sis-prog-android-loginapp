package br.com.fiap.si.receiveapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class Logger extends Service {
    public Logger() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void onCreate(){
        Log.i("ServicoMensagem", "Serviço iniciado...");
    }

    public void onDestroy(){
        Log.i("ServicoMensagem","Serviço encerrado...");
    }

}
