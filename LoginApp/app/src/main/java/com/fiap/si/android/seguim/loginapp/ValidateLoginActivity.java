package com.fiap.si.android.seguim.loginapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ValidateLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_login);

        Bundle params = getIntent().getExtras();

        String user = params.getString("login");
        String pass = params.getString(" ");

        Toast.makeText(this, user + "-" + pass, Toast.LENGTH_SHORT).show();

    }
}
